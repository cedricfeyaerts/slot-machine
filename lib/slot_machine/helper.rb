module SlotMachine
  # In the the view:
  # `= foo label: 'toto' do 'something awesome' end`
  #
  # In the partial foo:
  # `slots[:label] == 'toto'`
  # `slots[:main] == 'something awesome'`

  # create the module to be called as a helper
  # Typically :
  # ```
  #   helper SlotMachine.helper_module(my_path)
  # ````
  def self.helper_module(path: nil)
    # Read slot on the custom path
    slots = SlotMachine::Service.instance.read_slots(path)

    # Instantiate the new module
    @helper_module = Module.new

    # Add 1 method per partial in the module as well
    slots.each do |method_name, partial|
      @helper_module.module_eval do
        define_method(method_name) do |locals = {}, &block|
          SlotMachine::Service.instance.render_slot(self, partial, locals, &block)
        end
      end
    end

    @helper_module
  end
end
