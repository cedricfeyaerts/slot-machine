class SlotMachine::Service
  def self.instance
    return @instance unless @instance.nil?

    @instance = new
  end

  def render_slot(template, partial, locals = {}, &block)
    # that is some weird shit but il allow to access all the local_assigns
    # within the slots and still access the directly
    locals[:slots] = locals

    # Here we create the builder
    # we execute the block while passing the builder
    # all the slotted partial will be in the builder
    # and activeview buffer is captured and slotted
    # in the main slot
    if block
      builder = SlotMachine::Builder.new(template)
      captured = template.capture { yield(builder) }
      builder.append_slot(:main, captured)
      locals[:slots] = locals[:slots].merge(builder.slots)
      locals[:m] = builder
    end

    # Render le partial
    template.render partial, locals
  end

  def read_slots(custom_path = nil)
    globs = (custom_path || Rails.root.join('app/views/slots/_*'))
    globs = [globs] unless globs.is_a?(Array)

    paths_array = globs.map do |g|
      Dir[g]
    end.flatten

    slots_array = paths_array.map do |p|
      basename = File.basename(p, File.extname(p))
      basename = basename.split('.').first
      basename.slice!(0)
      method_name = basename.parameterize.underscore.to_sym
      partial_path = File.dirname(p).split('/views/').last
      partial = File.join(partial_path, basename)
      [method_name, partial]
    end

    Hash[slots_array]
  end
end
