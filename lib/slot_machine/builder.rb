# A builder class base for the builder of the different partials
class SlotMachine::Builder
  attr_internal :slots
  attr_accessor :output_buffer

  def initialize(template)
    @template = template
  end

  # append the named slot
  def append_slot(name = :main, string = nil, &block)
    slots[name].concat(get_buffer(string, &block))
    nil # why the fuck do I need this. It shouldn't put anything in the buffer but it does.
  end
  alias_method :slot, :append_slot

  # attribute reader
  def slots
    @slots ||= Hash.new { |h, k| h[k] = ActiveSupport::SafeBuffer.new('') }
  end

  # capture the block if ther is one
  def get_buffer(string = nil, &block)
    return string unless block

    @template.capture { yield }
  end
end
